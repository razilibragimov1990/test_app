// /**
//  * First we will load all of this project's JavaScript dependencies which
//  * includes Vue and other libraries. It is a great starting point when
//  * building robust, powerful web applications using Vue and Laravel.
//  */

// require('./bootstrap');

// window.Vue = require('vue').default;

// /**
//  * The following block of code may be used to automatically register your
//  * Vue components. It will recursively scan this directory for the Vue
//  * components and automatically register them with their "basename".
//  *
//  * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
//  */

// // const files = require.context('./', true, /\.vue$/i)
// // files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

// Vue.component('example-component', require('./components/ExampleComponent.vue').default);

// /**
//  * Next, we will create a fresh Vue application instance and attach it to
//  * the page. Then, you may begin adding components to this application
//  * or customize the JavaScript scaffolding to fit your unique needs.
//  */

// const app = new Vue({
//     el: '#app',
// });

require('./bootstrap');

window.Vue = require('vue').default;
import VueRouter from 'vue-router';
import App from './app.vue';
window.Vue.use(VueRouter);

import BidsIndex from './components/bids/BidsIndex.vue';
import BidsCreate from './components/bids/BidsCreate.vue';
import BidsEdit from './components/bids/BidsEdit.vue';

const routes = [
    {
        path: '/bids',
        component: BidsIndex,
        name: "BidsIndex"
    },
    {
        path: '/bids/create',
        component: BidsCreate,
        name: "BidsCreate"
    },
    {
        path: '/bids/edit/:id',
        component: BidsEdit,
        name: "BidsEdit"
    }
];

import axios from 'axios';
import VueAxios from 'vue-axios';

Vue.use(VueAxios, axios);

const router = new VueRouter({
    mode: 'history',
    routes: routes
});

const app = new Vue({
    el: '#app',
    router: router,
    render: h => h(App),
});